<!DOCTYPE html>
<html lang="zxx">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <title>infix Admin</title>
    <meta name="csrf-token" content="{{csrf_token()}}">

    <link rel="icon" href="{{asset('assets/admin/img/favicon.png ')}}" type="image/png">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('assets/admin/css/bootstrap.min.css')}}" />
    <!-- themefy CSS -->
    <link rel="stylesheet" href="{{asset('assets/admin/vendors/themefy_icon/themify-icons.css')}}" />
    <!-- swiper slider CSS -->
    <link rel="stylesheet" href="{{asset('assets/admin/vendors/swiper_slider/css/swiper.min.css')}}" />
    <!-- select2 CSS -->
    <link rel="stylesheet" href="{{asset('assets/admin/vendors/select2/css/select2.min.css ')}}" />
    <!-- select2 CSS -->
    <link rel="stylesheet" href="{{asset('assets/admin/vendors/niceselect/css/nice-select.css ')}}" />
    <!-- owl carousel CSS -->
    <link rel="stylesheet" href="{{asset('assets/admin/vendors/owl_carousel/css/owl.carousel.css ')}}" />
    <!-- gijgo css -->
    <link rel="stylesheet" href="{{asset('assets/admin/vendors/gijgo/gijgo.min.css ')}}" />
    <!-- font awesome CSS -->
    <link rel="stylesheet" href="{{asset('assets/admin/vendors/font_awesome/css/all.min.css ')}}" />
    <link rel="stylesheet" href="{{asset('assets/admin/vendors/tagsinput/tagsinput.css ')}}" />
    <!-- datatable CSS -->
    <link rel="stylesheet" href="{{asset('assets/admin/vendors/datatable/css/jquery.dataTables.min.css ')}}" />
    <link rel="stylesheet" href="{{asset('assets/admin/vendors/datatable/css/responsive.dataTables.min.css ')}}" />
    <link rel="stylesheet" href="{{asset('assets/admin/vendors/datatable/css/buttons.dataTables.min.css ')}}" />
    <!-- text editor css -->
    <link rel="stylesheet" href="{{asset('assets/admin/vendors/text_editor/summernote-bs4.css ')}}" />
    <!-- morris css -->
    <link rel="stylesheet" href="{{asset('assets/admin/vendors/morris/morris.css ')}}">
    <!-- metarial icon css -->
    <link rel="stylesheet" href="{{asset('assets/admin/vendors/material_icon/material-icons.css ')}}" />

    <!-- menu css  -->
    <link rel="stylesheet" href="{{asset('assets/admin/css/metisMenu.css ')}}">
    <!-- style CSS -->
    <link rel="stylesheet" href="{{asset('assets/admin/css/style.css ')}}" />
    @stack('css')
</head>
<div id="app">
    <!-- main content part here -->

    <!-- horizontal_menu  -->
    <div class="horizontal_menu">
        <div class="header_wrapper d-flex align-items-center justify-content-between">

            <div class="header_left">
                <div class="logo">
                    <a href="index.php">
                        <img src="{{asset('assets/admin/img/logo.png')}} " alt="">
                    </a>
                </div>
            </div>

            <div class="main_menu">
                <nav>
                    <ul>
                        <li><a href="index.php">Dashboard</a></li>
                        <li><a href="category_lists.php">Category</a></li>
                        <li><a href="course_list.php">Courses</a></li>
                        <li><a href="#">Examination <i class="fas fa-caret-down"></i></a>
                            <ul class="submenu">
                                <li>
                                    <a href="exam_categories.php">Categories</a>
                                </li>
                                <li>
                                    <a href="exam_question_subjects.php">Question Bank</a>
                                </li>
                                <li>
                                    <a href="exam_quizes.php">Quiz</a>
                                </li>
                                <li>
                                    <a href="exam_types.php">Types</a>
                                </li>
                                <li>
                                    <a href="exam_series.php">Series</a>
                                </li>
                                <li>
                                    <a href="exam_instructions.php">Instructions</a>
                                </li>
                                <li>
                                    <a href="exam_subject_master.php">Subject Master</a>
                                </li>
                                <li>
                                    <a href="exam_subject_topics.php">Subject Topics</a>
                                </li>
                            </ul>
                        </li>
                        <li><a href="payment_report.php">Payment</a></li>
                        <li><a href="student_list.php">Students</a></li>
                        <li><a href="#">Enrollment<i class="fas fa-caret-down"></i></a>
                            <ul class="submenu">
                                <li>
                                    <a href="enrollment.php">Enrollment</a>
                                </li>
                            </ul>
                        </li>
                        <li><a href="#">Report<i class="fas fa-caret-down"></i></a>
                            <ul class="submenu">
                                <li>
                                    <a href="admin_revenue.php"> Admin Revenue</a>
                                </li>
                                <li>
                                    <a href="report_instructor_revenue.php">Report Instructor</a>
                                </li>
                                <li>
                                    <a href="course_earning_report.php">Course Earning</a>
                                </li>
                                <li>
                                    <a href="course_complection_report.php">Course Complection</a>
                                </li>
                            </ul>
                        </li>
                        <li><a href="coupons.php">Coupons</a></li>
                        <li><a href="messages.php">Message</a></li>
                        <li><a href="#">Settings<i class="fas fa-caret-down"></i></a>
                            <ul class="submenu">
                                <li>
                                    <a href="system_settings.php">system settings</a>
                                </li>
                                <li>
                                    <a href="payment_settings.php">Payment Settings</a>
                                </li>
                                <li>
                                    <a href="course_fee_settings.php">Course Fee Settings</a>
                                </li>
                                <li>
                                    <a href="settings_language.php">Language Settings</a>
                                </li>
                                <li>
                                    <a href="website_settings.php">Website Settings</a>
                                </li>
                                <li>
                                    <a href="smtp_settings.php">smtp settings</a>
                                </li>
                                <li>
                                    <a href="pages.php">Pages</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>

            <div class="header_right">
                <ul>
                    <li><a href="#"> <span>Welcome Admin!</span> <img src="img/client_img.png" alt=""> </a>
                        <ul class="submenu">
                            <li>
                                <a href="#">My Profile</a>
                            </li>
                            <li>
                                <a href="system_settings.php">Settings</a>
                            </li>
                            <li>
                                <a href="#">Log Out</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- switcher_slide_wrapper  -->

    <div class="switcher_slide_wrapper">
        <div class="switcher_slide_wrapper_inner">
            <div class="spin_icon_clicker">
                <i class="fa fa-cog fa-spin"></i>
            </div>
            <h5>menu style</h5>
            <ul class="switcher_wrap">
                <li class="vertical active" > <i class="material-icons">
                        vertical_split
                    </i>
                    <span>vertical</span>
                </li>
                <li class="Horizontal"> <i class="material-icons">
                        horizontal_split</i>
                    <span>Horizontal</span>
                </li>
            </ul>
        </div>
    </div>
    <!-- sidebar  -->
    <!-- sidebar part here -->
    <nav class="sidebar">
        <div class="logo d-flex justify-content-between">
            <a href="index.php"><img src="img/logo.png" alt=""></a>
            <div class="sidebar_close_icon d-lg-none">
                <i class="ti-close"></i>
            </div>
        </div>
        <!-- admin_profile_Wrap  -->
        <div class="admin_profile_Wrap">
            <ul id="admin_profile_active">
                <li>
                    <a class="has-arrow" href="#" aria-expanded="false">
                        <div class="admin_profile_inner d-flex align-items-center">
                            <div class="thumb">
                                <img src="img/client_img.png" alt="">
                            </div>
                            <div class="welcome_rext">
                                <h5>Welcome Admin!</h5>
                                <span>Travor James</span>
                            </div>
                        </div>
                    </a>
                    <ul>
                        <li>
                            <a href="#">My Profile</a>
                        </li>
                        <li>
                            <a href="system_settings.php">Settings</a>
                        </li>
                        <li>
                            <a href="#">Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        <!--/ admin_profile_Wrap  -->
        <ul id="sidebar_menu">

            <li class="mm-active">
                <router-link  to="/admin/admin-dashboard">
                    <i class="material-icons"> dashboard </i>
                    <span>Dashboard</span>
                </router-link>
            </li>

            <li class=" ">
                <router-link  to="/admin/all-category">
                    <i class="material-icons">
                        category
                    </i>
                    <span>Category</span>
                </router-link>
            </li>

            <li class="">
                <router-link  to="/admin/all-course">
                    <!-- <i class="material-icons">
                    assignment
                    </i> -->
                    <i class="material-icons">
                        import_contacts
                    </i>
                    <span>Courses</span>
                </router-link>
            </li>

            <li class="metis_dropmenu">
                <a class="has-arrow" href="#" aria-expanded="false">
                    <i class="material-icons">
                        assignment
                    </i>
                    <span>Examination</span>
                </a>
                <ul>
                    <li>
                        <router-link to="/admin/exam/all_category"> Categories</router-link>
                    </li>
                    <li>
                        <router-link to="/admin/exam/question_bank">Question Bank</router-link>
                    </li>
                    <li>
                        <router-link to="/admin/exam/quiz"> Quiz</router-link>
                    </li>
                    <li>
                        <router-link to="/admin/exam/quiz-type">Types</router-link>
                    </li>
                    <li>
                        <router-link to="/admin/exam/series">Series</router-link>
                    </li>
                    <li>
                        <router-link to="/admin/exam/instruction">Instructions</router-link>
                    </li>
                    <li>
                        <router-link to="/admin/exam/subjectMaster">Subject Master</router-link>
                    </li>
                    <li>
                        <router-link to="/admin/exam/subjectTopics">Subject Topics</router-link>
                    </li>
                </ul>
            </li>
            <li class="">
                <router-link to="/admin/payment_report">
                    <i class="material-icons">
                        payment
                    </i>
                    <span>Payment</span>
                </router-link>
            </li>
            <li class="">
                <router-link to="/admin/all-student" aria-expanded="false">
                    <i class="material-icons">
                        supervised_user_circle
                    </i>
                    <span>Students</span>
                </router-link>


            </li>

            <li class="metis_dropmenu">
                <a class="has-arrow" href="#" aria-expanded="false">
                    <i class="material-icons">
                        games
                    </i>
                    <span>Enrollment</span>
                </a>
                <ul>
                    <li>
                        <a href="enrollment.php">Enrollment</a>
                    </li>
                </ul>
            </li>

            <li class="metis_dropmenu">
                <a class="has-arrow" href="#" aria-expanded="false">
                    <i class="material-icons">
                        donut_small
                    </i>
                    <span>Report</span>
                </a>
                <ul>
                    <li>
                        <a href="admin_revenue.php"> Admin Revenue</a>
                    </li>
                    <li>
                        <a href="report_instructor_revenue.php">Report Instructor</a>
                    </li>
                    <li>
                        <a href="course_earning_report.php">Course Earning</a>
                    </li>
                    <li>
                        <a href="course_complection_report.php">Course Complection</a>
                    </li>
                </ul>
            </li>

            <li class="">
               <router-link to="/admin/all_coupon">
                   <i class="material-icons">
                       card_giftcard
                   </i>
                   <span>Coupons</span>
               </router-link>


            </li>

            <li class="">
                <router-link to="/admin/all_message">
                    <i class="material-icons">
                        message
                    </i>
                    <span>Message</span>
                </router-link>
            </li>

            <li class="metis_dropmenu">
                <a class="has-arrow" href="#" aria-expanded="false">
                    <i class="material-icons">
                        brightness_5
                    </i>
                    <span>Settings</span>
                </a>
                <ul>
                    <li>
                        <a href="system_settings.php">system settings</a>
                    </li>
                    <li>
                        <a href="payment_settings.php">Payment Settings</a>
                    </li>
                    <li>
                        <a href="course_fee_settings.php">Course Fee Settings</a>
                    </li>
                    <li>
                        <a href="settings_language.php">Language Settings</a>
                    </li>
                    <li>
                        <a href="website_settings.php">Website Settings</a>
                    </li>
                    <li>
                        <a href="smtp_settings.php">smtp settings</a>
                    </li>
                    <li>
                        <a href="pages.php">Pages</a>
                    </li>
                </ul>
            </li>
        </ul>

    </nav>
    <!-- sidebar part end -->
<admin_master></admin_master>
<!-- main content part end -->

</div>


<!-- jquery slim -->
<script src="{{asset('assets/admin/js/jquery-3.4.1.min.js')}} "></script>

<script src="{{asset('js/app.js')}}"></script>
<!-- popper js -->
<script src="{{asset('assets/admin/js/popper.min.js ')}}"></script>
<!-- bootstarp js -->
<script src="{{asset('assets/admin/js/bootstrap.min.js ')}}"></script>
<!-- sidebar menu  -->
<script src="{{asset('assets/admin/js/metisMenu.js ')}}"></script>
<!-- waypoints js -->
<script src="{{asset('assets/admin/vendors/count_up/jquery.waypoints.min.js ')}}"></script>
<!-- waypoints js -->
<script src="{{asset('assets/admin/vendors/chartlist/Chart.min.js ')}}"></script>
<!-- counterup js -->
<script src="{{asset('assets/admin/vendors/count_up/jquery.counterup.min.js ')}}"></script>
<!-- swiper slider js -->
<script src="{{asset('assets/admin/vendors/swiper_slider/js/swiper.min.js ')}}"></script>
<!-- nice select -->
<script src="{{asset('assets/admin/vendors/niceselect/js/jquery.nice-select.min.js ')}}"></script>
<!-- owl carousel -->
<script src="{{asset('assets/admin/vendors/owl_carousel/js/owl.carousel.min.js ')}}"></script>
<!-- gijgo css -->
<script src="{{asset('assets/admin/vendors/gijgo/gijgo.min.js ')}}"></script>
<!-- responsive table -->
<script src="{{asset('assets/admin/vendors/datatable/js/jquery.dataTables.min.js ')}}"></script>
<script src="{{asset('assets/admin/vendors/datatable/js/dataTables.responsive.min.js ')}}"></script>
<script src="{{asset('assets/admin/vendors/datatable/js/dataTables.buttons.min.js ')}}"></script>
<script src="{{asset('assets/admin/vendors/datatable/js/buttons.flash.min.js ')}}"></script>
<script src="{{asset('assets/admin/vendors/datatable/js/jszip.min.js ')}}"></script>
<script src="{{asset('assets/admin/vendors/datatable/js/pdfmake.min.js ')}}"></script>
<script src="{{asset('assets/admin/vendors/datatable/js/vfs_fonts.js ')}}"></script>
<script src="{{asset('assets/admin/vendors/datatable/js/buttons.html5.min.js ')}}"></script>
<script src="{{asset('assets/admin/vendors/datatable/js/buttons.print.min.js ')}}"></script>
<script src="{{asset('assets/admin/js/raphael-min.js')}}"></script>
<script src="{{asset('assets/admin/js/morris/morris.min.js ')}}"></script>
<script src="{{asset('assets/admin/js/chart/bar-chart.js ')}}"></script>

<!-- progressbar js -->
<script src="{{asset('assets/admin/vendors/progressbar/jquery.barfiller.js ')}}"></script>
<!-- tag input -->
<script src="{{asset('assets/admin/vendors/tagsinput/tagsinput.js ')}}"></script>
<!-- text editor js -->
<script src="{{asset('assets/admin/vendors/text_editor/summernote-bs4.js ')}}"></script>

<!-- custom js -->
<script src="{{asset('assets/admin/js/custom.js ')}}"></script>

<!-- active_chart js -->
<script src="{{asset('assets/admin/js/active_chart.js ')}}"></script>

<!-- active_chart js -->
<script src="{{asset('assets/admin/js/chart/pie.js ')}}"></script>



@stack('js')

</body>
</html>