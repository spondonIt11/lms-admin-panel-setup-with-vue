
require('./bootstrap');

window.Vue = require('vue');

import VueRouter from 'vue-router'
Vue.use(VueRouter);
import {routes} from "./adminRouter";


Vue.component('admin_master', require('./components/admin/adminMaster.vue').default);


const router = new VueRouter({
    routes ,
    mode: 'hash'
});

const app = new Vue({
    el: '#app',
    router
});
