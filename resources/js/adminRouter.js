import dashboard from './components/admin/dashboard.vue'
import all_student from './components/admin/student/index.vue'
import all_category from './components/admin/category/index.vue'
import all_course from './components/admin/course/index.vue'
import payment_report from './components/admin/report/payment_report.vue'
import coupon_all from './components/admin/coupon/index'
import message_all from './components/admin/message/index'
import examAllCategory from './components/admin/exam/index'
import questionBank from "./components/admin/exam/questionBank";
import quiz from "./components/admin/exam/quiz";
import quizType from "./components/admin/exam/quizType";
import series from "./components/admin/exam/series";
import instruction from "./components/admin/exam/instruction";
import subjectMaster from "./components/admin/exam/subjectMaster";
import subjectTopics from "./components/admin/exam/subjectTopics";

export const routes = [
    {
        path: '/admin/admin-dashboard',
        component:dashboard
    },
    {
        path: '/admin/all-student',
        component:all_student
    },
    {
        path: '/admin/all-category',
        component:all_category
    },

    {
        path: '/admin/all-course',
        component:all_course
    },

    {
        path: '/admin/payment_report',
        component:payment_report
    },
    {
        path: '/admin/all_coupon',
        component:coupon_all
    },

    {
        path: '/admin/all_message',
        component:message_all
    },

    {
        path: '/admin/exam/all_category',
        component:examAllCategory
    },
    {
        path: '/admin/exam/question_bank',
        component:questionBank
    },

    {
        path: '/admin/exam/quiz',
        component:quiz
    },
    {
        path: '/admin/exam/quiz-type',
        component:quizType
    },
    {
        path: '/admin/exam/series',
        component:series
    },
    {
        path: '/admin/exam/instruction',
        component:instruction
    },
    {
        path: '/admin/exam/subjectMaster',
        component:subjectMaster
    },
    {
        path: '/admin/exam/subjectTopics',
        component:subjectTopics
    },



];