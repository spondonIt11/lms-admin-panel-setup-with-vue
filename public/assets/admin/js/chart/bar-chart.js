(function ($) {
    "use strict";



    $(document).ready(function() {
        Morris.Bar({
            element: 'morris_bar',
            data: [{
                y: 'Dec',
                a: '140000', 
            }, {
                y: 'Nov',
                a: 250000, 
            }, {
                y: 'Oct',
                a: 100000, 
            }, {
                y: 'Sep',
                a: 170005, 
            }, {
                y: 'Aug',
                a: 100000, 
            }, {
                y: 'Jul',
                a: 160000, 
            }, {
                y: 'Jun',
                a: 205000, 
            }, {
                y: 'May',
                a: 203000, 
            }, {
                y: 'Apr',
                a: 120040, 
            }, {
                y: 'Mar',
                a: 233009, 
            }, {
                y: 'Feb',
                a: 145000, 
            }, {
                y: 'Jan',
                a: 103000, 
            }],
            xkey: 'y',
            ykeys: ['a'],
            barGap:3,
            labels: ['$'],
            barColors: ['#fe1724'],
            hideHover: 'auto',
            gridLineColor: '#eee1e2',
            resize: true,
            barSizeRatio: 0.15,
            stacked: true, 
            behaveLikeLine: true, 
            hoverCallback: function (index, options, content, row) {
                return  '<span>' + row.y + ' income' +'</span>' +' <br>'+ '$'+ row.a;
              }
        });
        Morris.Bar({
            element: 'morris_bar1',
            data: [{
                y: 'Dec',
                a: '140000', 
            }, {
                y: 'Nov',
                a: 250000, 
            }, {
                y: 'Oct',
                a: 100000, 
            }, {
                y: 'Sep',
                a: 170005, 
            }, {
                y: 'Aug',
                a: 100000, 
            }, {
                y: 'Jul',
                a: 160000, 
            }, {
                y: 'Jun',
                a: 205000, 
            }, {
                y: 'May',
                a: 203000, 
            }, {
                y: 'Apr',
                a: 120040, 
            }, {
                y: 'Mar',
                a: 233009, 
            }, {
                y: 'Feb',
                a: 145000, 
            }, {
                y: 'Jan',
                a: 103000, 
            }],
            xkey: 'y',
            ykeys: ['a'],
            barGap:3,
            labels: ['$'],
            barColors: ['#fe1724'],
            hideHover: 'auto',
            gridLineColor: '#eee1e2',
            resize: true,
            barSizeRatio: 0.15,
            stacked: true, 
            behaveLikeLine: true, 
            hoverCallback: function (index, options, content, row) {
                return  '<span>' + row.y + ' income' +'</span>' +' <br>'+ '$'+ row.a;
              }
        });
    });

    $(document).ready(function() {

        Morris.Bar({
            element: 'morris_bar_bold',
            data: [{
                y: 'Completed',
                a: 25, 
            }, {
                y: 'Cancelled',
                a: 15, 
            },{
                y: 'Pending',
                a: 15, 
            }],
            xkey: 'y',
            ykeys: ['a'],
            labels: ['Calls'],
            hideHover: 'always',
            barColors: function (row, series, type) {
            console.log("--> "+row.label, series, type);
            if(row.label == "Completed") return "#0dd503";
            else if(row.label == "Cancelled") return "#fe1724";
            else if(row.label == "Pending") return "#fedb17";
            },
            xkey: 'y',
            ykeys: ['a'],
            barGap:1,
            labels: [' '],
            hideHover: 'auto',
            resize: true,
            barSizeRatio: 0.80,

            hoverCallback: function (index, options, content, row) {
                return row.a +' <br>'+'<span>' + row.y + ' payment' +'</span>';
              }
        });

        Morris.Bar({
            element: 'morris_bar_bold1',
            data: [{
                y: 'Completed',
                a: 25, 
            }, {
                y: 'Cancelled',
                a: 15, 
            },{
                y: 'Pending',
                a: 15, 
            }],
            xkey: 'y',
            ykeys: ['a'],
            labels: ['Calls'],
            hideHover: 'always',
            barColors: function (row, series, type) {
            console.log("--> "+row.label, series, type);
            if(row.label == "Completed") return "#0dd503";
            else if(row.label == "Cancelled") return "#fe1724";
            else if(row.label == "Pending") return "#fedb17";
            },
            xkey: 'y',
            ykeys: ['a'],
            barGap:1,
            labels: [' '],
            hideHover: 'auto',
            resize: true,
            barSizeRatio: 0.80,

            hoverCallback: function (index, options, content, row) {
                return row.a +' <br>'+'<span>' + row.y + ' payment' +'</span>';
              }
        });
    });



}(jQuery));