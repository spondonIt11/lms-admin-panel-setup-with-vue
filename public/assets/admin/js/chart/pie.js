(function ($) {
  "use strict";
  //pie chart

  
  const ctx = document.getElementById("myChart").getContext('2d');
  ctx.height = 100;

Chart.Legend.prototype.afterFit = function() {
  this.height = this.height + 37;
};
  new Chart(ctx, {
      type: 'doughnut',
      data: {
          defaultFontFamily: 'Roboto", sans-serif',
          datasets: [{
              data: [45, 35],
              backgroundColor: [
                "#128fec",
                "#f9001c"
              ]

          }],
          labels: [
              "Active Courses",
              "Pending Courses",
          ]
      },
      
      options: {
        tooltips: {
          caretSize: 0,
        // titleAlign: 'center',
        //   bodyAlign: 'center',
          callbacks: {
            title: function(tooltipItem, data) {
              return data['labels'][tooltipItem[0]['index']];
            },
            label: function(tooltipItem, data) {
              return data['datasets'][0]['data'][tooltipItem['index']];
            },
            afterLabel: function(tooltipItem, data) {
              var dataset = data['datasets'][0];
              var percent = Math.round((dataset['data'][tooltipItem['index']] / dataset["_meta"][0]['total']));
            }
          },
          backgroundColor: '#000',
          titleFontSize: 12,
          titleFontColor: '#7e7172',
          bodyFontColor: '#fff',
          bodyFontSize: 12,
          displayColors: false
        }
      }
  });
  
})(jQuery);

// var ctx = document.getElementById("myChart");
// var myChart = new Chart(ctx, {
//   type: 'doughnut',
//   data: {
//     labels: ["Active Courses", "Pending Courses", "Yellow"],
//     datasets: [{
//       label: '# of Votes',
//       data: [12, 19, 3],
//       backgroundColor: [
//         'rgba(255, 99, 132, 0.2)',
//         'rgba(54, 162, 235, 0.2)',
//         'rgba(255, 206, 86, 0.2)'
//       ],
//       borderColor: [
//         'rgba(255,99,132,1)',
//         'rgba(54, 162, 235, 1)',
//         'rgba(255, 206, 86, 1)'
//       ],
//       borderWidth: 1
//     }]
//   },
//   options: {
//     tooltips: {
//       callbacks: {
//         title: function(tooltipItem, data) {
//           return data['labels'][tooltipItem[0]['index']];
//         },
//         label: function(tooltipItem, data) {
//           return data['datasets'][0]['data'][tooltipItem['index']];
//         },
//         afterLabel: function(tooltipItem, data) {
//           var dataset = data['datasets'][0];
//           var percent = Math.round((dataset['data'][tooltipItem['index']] / dataset["_meta"][0]['total']) * 100)
//           return '(' + percent + '%)';
//         }
//       },
//       backgroundColor: '#FFF',
//       titleFontSize: 16,
//       titleFontColor: '#0066ff',
//       bodyFontColor: '#000',
//       bodyFontSize: 14,
//       displayColors: false
//     }
//   }
// });